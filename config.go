package mastodon

// Config holds the config. data for the Postgres connection.
type Config struct {
	PostgresHost               string
	PostgresPort               uint16
	PostgresDatabase           string
	PostgresUser               string
	PostgresPassword           string
	PostgresMaxConnections     int
	PostgresMaxIdleConnections int
}
