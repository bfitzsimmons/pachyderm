package mastodon

import (
	"errors"
	"net/http"

	"goji.io"
	"golang.org/x/net/context"
)

type contextKey string

const (
	// PostgresKey is the context key used to store the Postgres connection pool.
	PostgresKey contextKey = "pg"
)

// Errors
var (
	ErrMissingPGContext = errors.New("PG missing from context")
)

// NewPGContext returns a context containing the postgres connection pool.
func NewPGContext(ctx context.Context, pg *Postgres) context.Context {
	return context.WithValue(ctx, PostgresKey, pg)
}

// PGFromContext returns a clients container from the context.
func PGFromContext(ctx context.Context) (*Postgres, bool) {
	pg, ok := ctx.Value(PostgresKey).(*Postgres)
	return pg, ok
}

// ContextMiddleware adds Postgres to the request context.
func ContextMiddleware(next goji.Handler) goji.Handler {
	fn := func(ctx context.Context, w http.ResponseWriter, r *http.Request) {
		ctx = NewPGContext(ctx, PG)
		next.ServeHTTPC(ctx, w, r)
	}
	return goji.HandlerFunc(fn)
}
