package mastodon

import (
	"fmt"
	"strings"

	"github.com/fatih/structs"
)

// ModelData is metadata from a model.
type ModelData struct {
	Model       Model
	FieldNames  []string
	FieldValues []interface{}
}

// Process extracts metadata from the passed model. We only want to do this once, so we're getting as much from the
// model as we can in a single pass.
func (mfd *ModelData) Process() {
	s := structs.New(mfd.Model)
	for _, field := range s.Fields() {
		// Don't use the ID field.
		if field.Name() == "ID" {
			continue
		}

		// Get the struct field tag.
		tag := field.Tag("db")

		// Skip fields with empty tags.
		if tag == "" || tag == "-" {
			continue
		}

		mfd.FieldNames = append(mfd.FieldNames, tag)
		mfd.FieldValues = append(mfd.FieldValues, field.Value())
	}
}

// FieldNamesToString returns a comma separated string containing the struct field names.
func (mfd *ModelData) FieldNamesToString() string {
	output := ""
	for _, name := range mfd.FieldNames {
		output += fmt.Sprintf("%s, ", name)
	}
	return strings.TrimSuffix(output, ", ")
}

// // GetCheckValues returns a slice of the values that match the existence check fields.
// func (mfd *ModelData) GetCheckValues() []interface{} {
// 	values := []interface{}{}
// 	for _, field := range mfd.Model.GetCheckFields() {
// 		for i, item := range mfd.FieldNames {
// 			if field == item {
// 				values = append(values, mfd.FieldValues[i])
// 			}
// 		}
// 	}
// 	return values
// }
