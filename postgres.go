package mastodon

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/nleof/goyesql"
)

var (
	// PG contains the postgres connection pool.
	PG *Postgres

	// CommonQueries contains common sql queries.
	CommonQueries goyesql.Queries
)

func init() {
	data := MustAsset("sql/common.sql")
	CommonQueries = goyesql.MustParseBytes(data)
}

// Model is the interface for checking for the existence of objects in the db.
type Model interface {
	// TableName() string
	// GetID() int32
	// GetCheckFields() []string
	// Map(values []interface{}) error
}

// Postgres wraps the Postgres client so that functionality may be added if needed.
type Postgres struct {
	*sqlx.DB
}

// InitializePostgres initializes the connections to the Postgres host.
func InitializePostgres(config *Config) (*Postgres, error) {
	dataSourceName := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.PostgresHost,
		config.PostgresPort,
		config.PostgresUser,
		config.PostgresPassword,
		config.PostgresDatabase,
	)

	// Make the connection.
	connection, err := sqlx.Connect("postgres", dataSourceName)
	if err != nil {
		return nil, err
	}

	// Set the connection limits.
	connection.SetMaxIdleConns(config.PostgresMaxIdleConnections)
	connection.SetMaxOpenConns(config.PostgresMaxConnections)

	return &Postgres{connection}, nil
}

// // RowCreate inserts an object into the db.
// func (pg *Postgres) RowCreate(m Model) *pgx.Row {
// 	// Get field data from the model.
// 	modelData := &ModelData{Model: m}
// 	modelData.Process()
//
// 	// Prepare the INSERT statement.
// 	sql := fmt.Sprintf(CommonQueries["create"],
// 		modelData.Model.TableName(),
// 		modelData.FieldNamesToString(),
// 		GeneratePlaceholders(len(modelData.FieldNames)),
// 	)
//
// 	// Run the query.
// 	return pg.QueryRow(sql, modelData.FieldValues...)
// }

// // RowExists tests for the existence of the object in the db.
// func (pg *Postgres) RowExists(m Model) (exists bool, err error) {
// 	// Get field data from the model.
// 	modelData := &ModelData{Model: m}
// 	modelData.Process()
//
// 	// Prepare the SELECT statement.
// 	sql := fmt.Sprintf(CommonQueries["exists"], modelData.Model.TableName(), GenerateCheckWhereClause(m))
//
// 	// Run the query and scan the result into our boolean.
// 	err = pg.QueryRow(sql, modelData.GetCheckValues()...).Scan(&exists)
//
// 	return exists, err
// }

// // RowDelete deletes the object from the db.
// func (pg *Postgres) RowDelete(m Model) (pgx.CommandTag, error) {
// 	// Prepare the DELETE statement.
// 	sql := fmt.Sprintf(CommonQueries["delete"], m.TableName())
//
// 	// Run the query.
// 	return pg.Exec(sql, m.GetID())
// }

// // RowRead retrieves a single object row from the db.
// func (pg *Postgres) RowRead(m Model) *pgx.Row {
// 	// Prepare the SELECT statement.
// 	sql := fmt.Sprintf(CommonQueries["read"], m.TableName())
//
// 	// Run the query.
// 	return pg.QueryRow(sql, m.GetID())
// }
