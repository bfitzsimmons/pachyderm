-- common.sql

-- name: list
SELECT * FROM %s ORDER BY id;

-- name: create
INSERT INTO %s (%s) VALUES(%s) RETURNING id;

-- name: read
SELECT * FROM %s WHERE id = $1 LIMIT 1;

-- name: update
UPDATE %s SET %s WHERE id = %s;

-- name: delete
DELETE FROM %s WHERE id = $1;

-- name: exists
SELECT EXISTS(SELECT 1 FROM %s WHERE %s) AS "exists";

-- name: count
SELECT COUNT(*) FROM %s WHERE %s;
