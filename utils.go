package mastodon

import (
	"fmt"
	"strings"
)

// // GenerateCheckWhereClause generates the WHERE clause to be used in the object existence check.
// func GenerateCheckWhereClause(m Model) string {
// 	output := ""
// 	for i, field := range m.GetCheckFields() {
// 		output += fmt.Sprintf("%s = $%d AND ", field, i+1)
// 	}
// 	return strings.TrimSuffix(output, " AND ")
// }

// GeneratePlaceholders returns the field placeholders for use in SQL queries.
func GeneratePlaceholders(num int) string {
	placeholders := ""
	for i := 0; i < num; i++ {
		placeholders += fmt.Sprintf("$%d, ", i+1)
	}
	return strings.TrimSuffix(placeholders, ", ")
}
