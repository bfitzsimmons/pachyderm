# Mastodon
Mastodon is a Postgres library written in Go. It will allow the user to connect to and interact with a Postgres DB.

## Compatability
This package is being written to work with the [Goji](http://goji.io/) Go microframework, and thus, may not be compatible with other frameworks, or lack thereof. At this point, compatiblity is not my primary concern, but we'll see how things go.

## Installation
Add the `-u` flag to the `get` command if you intend to update the library.
```bash
go get bitbucket.org/bfitzsimmons/mastodon
```

## Code Generation
### Binary data for static assets
The following requires [go-bindata](https://github.com/jteeuwen/go-bindata).
```bash
cd $GOPATH/src/bitbucket.org/bfitzsimmons/mastodon
go-bindata -pkg="mastodon" -ignore=.DS_Store sql/...
```